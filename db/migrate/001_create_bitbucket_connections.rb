# Copyright (C) 2017  Productize
#
# License: see LICENSE file

class CreateBitbucketConnections < Rails.version < '5.2' ? ActiveRecord::Migration : ActiveRecord::Migration[4.2]
  def self.up
    create_table :bitbucket_connections do |t|
      t.string :client_key
      t.string :shared_secret
      t.string :base_api_url
      t.string :user
      t.string :user_link
      t.string :principal
      t.string :principal_link
      t.boolean :allowed
    end
  end

  def self.down
    drop_table :bitbucket_connections
  end
end
